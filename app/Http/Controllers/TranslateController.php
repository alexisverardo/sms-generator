<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;

class TranslateController extends Controller
{
    public $translate = [
        'a' => ['ᴀ','ⓐ','ａ'],
        'b' => ['ʙ','ⓑ','ｂ'],
        'c' => ['ᴄ','ⓒ','ｃ'],
        'd' => ['ᴅ','ⓓ','ｄ'],
        'e' => ['ᴇ','ⓔ','ｅ'],
        'f' => ['ꜰ','ⓕ','ｆ'],
        'g' => ['ɢ','ⓖ','ｇ'],
        'h' => ['ʜ','ⓗ','ｈ'],
        'i' => ['ɪ','ⓘ','ｉ'],
        'j' => ['ᴊ','ⓙ','ｊ'],
        'k' => ['ᴋ','ⓚ','ｋ'],
        'l' => ['ʟ','ⓛ','ｌ'],
        'm' => ['ᴍ','ⓜ','ｍ'],
        'n' => ['ɴ','ⓝ','ｎ'],
        'ñ' => ['ñ','ñ','ñ'],
        'o' => ['ᴏ','ⓞ','ｏ'],
        'p' => ['ᴩ','ⓟ','ｐ'],
        'q' => ['q','ⓠ','ｑ'],
        'r' => ['ʀ','ⓡ','ｒ'],
        's' => ['ꜱ','ⓢ','ｓ'],
        't' => ['ᴛ','ⓣ','ｔ'],
        'u' => ['ᴜ','ⓤ','ｕ'],
        'v' => ['ᴠ','ⓥ','ｖ'],
        'w' => ['ᴡ','ⓦ','ｗ'],
        'x' => ['x','ⓧ','ｘ'],
        'y' => ['y','ⓨ','ｙ'],
        'z' => ['ᴢ','ⓩ','ｚ'],
        '0' => ['0','0','０'],
        '1' => ['1','①','１'],
        '2' => ['2','②','２'],
        '3' => ['3','③','３'],
        '4' => ['4','④','４'],
        '5' => ['5','⑤','５'],
        '6' => ['6','⑥','６'],
        '7' => ['7','⑦','７'],
        '8' => ['8','⑧','８'],
        '9' => ['9','⑨','９'],
    ];



    public function index() {
        $stringConvertArray = null;
        return view('translate.index');
    }

    public function create(Request $request) {
       $string = (string)$request->text;
       $string = strtolower($string);

        $stringConvert = '';
        $stringConvertArray = new Collection();
        $stringDiv = explode(' ' , $string);
        for($k=0;$k<10;$k++) {
            for ($j=0;$j<count($stringDiv);$j++) {
                $type = rand( 0 , 2);
                for($i=0;$i<strlen($stringDiv[$j]);$i++){
                    $keyExist = false;
                    foreach ($this->translate as $key => $value) {
                        if($stringDiv[$j][$i] == (string) $key) {
                            $stringConvert = $stringConvert . $value[$type];
                            $keyExist = true;
                            break;
                        }
                    }
                    if(!$keyExist) {
                        $stringConvert = $stringConvert . $stringDiv[$j][$i];
                    }
                }
                if($i !== strlen($stringDiv[$j]) - 1) {
                    $stringConvert = $stringConvert . ' ';
                }
            }
//            $stringConvertUni = html_entity_decode($stringConvert, ENT_QUOTES, 'UTF-8');
            $stringConvertArray->push(['value' => $stringConvert, 'length' => mb_strlen($stringConvert, 'UTF-16')]);
            $stringConvert = '';
        }
        //CIRCLE(BLANCA)
        for ($j=0;$j<count($stringDiv);$j++) {
            for($i=0;$i<strlen($stringDiv[$j]);$i++){
                $keyExist = false;
                foreach ($this->translate as $key => $value) {
                    if($stringDiv[$j][$i] == (string) $key) {
                        $stringConvert = $stringConvert . $value[1];
                        $keyExist = true;
                        break;
                    }
                }
                if(!$keyExist) {
                    $stringConvert = $stringConvert . $stringDiv[$j][$i];
                }
            }
            if($i !== strlen($stringDiv[$j]) - 1) {
                $stringConvert = $stringConvert . ' ';
            }
        }

        $stringConvertArray->push(['value' => $stringConvert, 'length' => mb_strlen($stringConvert, 'UTF-16')]);
        $stringConvert = '';

        //small caps
        for ($j=0;$j<count($stringDiv);$j++) {
            for($i=0;$i<strlen($stringDiv[$j]);$i++){
                $keyExist = false;
                foreach ($this->translate as $key => $value) {
                    if($stringDiv[$j][$i] == (string) $key) {
                        $stringConvert = $stringConvert . $value[0];
                        $keyExist = true;
                        break;
                    }
                }
                if(!$keyExist) {
                    $stringConvert = $stringConvert . $stringDiv[$j][$i];
                }
            }
            if($i !== strlen($stringDiv[$j]) - 1) {
                $stringConvert = $stringConvert . ' ';
            }
        }

        $stringConvertArray->push(['value' => $stringConvert, 'length' => mb_strlen($stringConvert, 'UTF-16')]);
        $stringConvert = '';



        //FULLWIDTH
        for ($j=0;$j<count($stringDiv);$j++) {
            for($i=0;$i<strlen($stringDiv[$j]);$i++){
                $keyExist = false;
                foreach ($this->translate as $key => $value) {
                    if($stringDiv[$j][$i] == (string) $key) {
                        $stringConvert = $stringConvert . $value[2];
                        $keyExist = true;
                        break;
                    }
                }
                if(!$keyExist) {
                    $stringConvert = $stringConvert . $stringDiv[$j][$i];
                }
            }
            if($i !== strlen($stringDiv[$j]) - 1) {
                $stringConvert = $stringConvert . ' ';
            }
        }

        $stringConvertArray->push(['value' => $stringConvert, 'length' => mb_strlen($stringConvert, 'UTF-16')]);
        $stringConvert = '';
        return view('translate.index', compact('stringConvertArray'));
    }

    public function translate(Request $request) {
        $string = (string)$request->text;
        $string = strtolower($string);
        $stringTranslate = '';
        $stringDiv = explode(' ' , $string);
        foreach($stringDiv as $keyW => $word) {
            foreach ($this->str_split_unicode($word) as $item) {
                $itemExist = false;
                foreach ($this->translate as $key => $l) {
                    foreach ($l as $type) {
                        if($type === $item) {
                            $stringTranslate = $stringTranslate . $key;
                            $itemExist = true;
                            break;
                        }
                    }
                }
                if(!$itemExist) {
                    $stringTranslate = $stringTranslate . $item;
                }
            }

            if($keyW !== strlen($word) - 1) {
                $stringTranslate = $stringTranslate . ' ';
            }
        }

        return view('translate.index', compact('stringTranslate'));
    }
    function str_split_unicode($str, $length = 1) {
        $tmp = preg_split('~~u', $str, -1, PREG_SPLIT_NO_EMPTY);
        if ($length > 1) {
            $chunks = array_chunk($tmp, $length);
            foreach ($chunks as $i => $chunk) {
                $chunks[$i] = join('', (array) $chunk);
            }
            $tmp = $chunks;
        }
        return $tmp;
    }
}
