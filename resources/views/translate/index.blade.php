@extends('layouts.default')
@section('content')
    <br>
    <br>
    <div class="container">
        <br>
        <br>
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Crear mensaje</div>
                    <div class="card-body">
                        <form action="{{ route('text.create')}}" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                                <label for="titulo">Texto</label>
                                <input type="textarea" class="form-control" id="text" name="text" required>
                            </div>
                            <button type="submit" class="btn btn-primary">Aceptar</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <br>
        @if (isset($stringConvertArray))

            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header">Mensajes</div>
                        <div class="card-body">
                            <ul>
                                @foreach ($stringConvertArray->all() as $stringConvert)
                                    <li>
                                        {{$stringConvert['value']}}  ||  {{$stringConvert['length']}}
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
    </div>
    @endif
    </div>
    <br>
    <div class="container">
        <br>
        <br>
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Traducir unicode</div>
                    <div class="card-body">
                        <form action="{{ route('text.translate')}}" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                                <label for="titulo">Texto</label>
                                <input type="textarea" class="form-control" id="text" name="text" required>
                            </div>
                            <button type="submit" class="btn btn-primary">Aceptar</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <br>
        @if (isset($stringTranslate))

            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header">Traduccion</div>
                        <div class="card-body">
                            <ul>
                                <li>
                                    {{$stringTranslate}}
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
    </div>
        @endif
    </div>
@stop
