<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/global-test', 'TranslateController@index');
Route::post('/global-test/create',  'TranslateController@create')->name('text.create');
Route::post('/global-test/translate', 'TranslateController@translate')->name('text.translate');
